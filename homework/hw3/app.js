
/**
 * Module dependencies.
 */

var express = require('express')
  , routes = require('./routes')
  , links = require('./routes/links')
  , http = require('http')
  , path = require('path')
  , mongoose = require('mongoose')
  , flash = require('connect-flash');
  

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.compress());
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.cookieParser());
app.use(express.session({secret:"laskjfflsdkajfdlskjflaksdjfkj"}));
app.use(flash());
app.use(function(req,res,next){
  res.locals.flash = {
    error:req.flash('error'),
    notice:req.flash('notice')
  };
  next();
});
app.use(express.static(path.join(__dirname, 'public')));
app.use(app.router);


// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

//db connection
mongoose.connect(process.env.IP);
mongoose.connection.on('connected',function(){
  console.log("Connected to MongoDB @" + process.env.IP);
});
mongoose.connection.on('disconnected',function(){
  console.log('Disconnected from MongoDB');
});
process.on('SIGINT',function(){
  mongoose.connection.close(function(){
    console.log('MongoDB disconnected due to Application Exit');
    process.exit(0);
  });
});

app.get('/', routes.index);
app.get('/links', links.index);
app.get('/links/edit', links.edit);
app.post('/links/edit', links.edit);
app.get('/links/add', links.add);
app.post('/links/add', links.add);
app.get('/links/delete', links.delete);

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
