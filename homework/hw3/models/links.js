//links models

var mongoose = require('mongoose')
    , Schema = mongoose.Schema
    , ObjectId = Schema.ObjectId;
    
var LinkSchema = new Schema({
    name:{type:String,
        required:true
    },
    category:{
        required:true,
        type:Object,
        enum:categories
    },
    description:{type:String},
    url:{type:String,
        required:true
    }
});
    
var Link = mongoose.model('project', LinkSchema);
exports.Link = Link;

/* with this I only have to change categories in one place */
var categories = [{label:"Bass Solo", value:"Bass Solo"},
                    {label:"For Mom", value:"For Mom"},
                    {label:"School", value:"School"},
                    {label:"Work", value:"Work"},
                    {label:"Not Pron", value:"Not Pron"},
                    {label:"Other", value:"Other"}];
                    
exports.categories = categories;