//links routing
var Link = require("../models/links").Link,
    categories = require("../models/links").categories;

// routing for /links
exports.index = function(req, res){
    Link.find({}, function(err, docs){
        if(err){
            console.log(err);
            docs = [];
        }
        //how to display category label instead of value
        res.render("links/index", {
            title:"View Links",
            categories:categories,
            links:docs
        });
    });
};

// routing for /links/add
exports.add = function(req, res){
    //if submitting the form
    if(req.method === "POST"){
        var newLink = new Link({
            name:req.body.name,
            description:req.body.description,
            url:req.body.url,
            category:req.body.category
        }).save(function(err){
            //if it fails validation
            if(err){
                console.log(err);
                //re-render the page with what data they did populate
                res.render("links/add", {title:"Add a new link", 
                    flash:{error:"Error saving link. Was there an empty required field?", notice:""},
                    categories:categories,
                    name:req.body.name, 
                    category:req.body.category,
                    description:req.body.description, 
                    url:req.body.url});
            }
            else{
                req.flash("notice", req.body.name + " added successfully.");
                res.redirect("/links"); 
            }
        });
    }
    //render the default add page
    else{
        res.render("links/add", {title:"Add a new link", name:"", categories:categories, category:"category1", description:"", url:"http://"});
    }
};

// routing for /links/edit?_id=XXXXXXXXXXXX
exports.edit = function(req, res){
    if(typeof req.query._id !== "undefined"){
        Link.find({_id:req.query._id}, function(err, docs){
            if(err){
                console.log("Ruh roh! " + err);
                req.flash("error", "Error editing link. Please try again.");
                res.redirect("/links");
            }
            else{
                if(docs.length !== 1){
                    console.log("oops? found 0 or multiple records with _id: " + req.query._id);
                }
                var doc=docs[0];
                
                //if submitting the form
                if(req.method === "POST"){
                    doc.name=req.body.name;
                    doc.description = req.body.description;
                    doc.url = req.body.url; 
                    doc.category = req.body.category;
                    doc.save(function(err){
                        //if it fails validation
                        if(err){
                            console.log(err);
                            //re-render the page with what data they did populate
                            res.render("links/edit", {flash:{error:"Error saving link. Were all required fields filled?", notice:""}, 
                                                        link:doc, 
                                                        categories:categories});
                        }
                        else{
                            req.flash("notice", req.body.name + " saved successfully.");
                            res.redirect("/links");
                        }
                    });
                }
                //render page with data from the link they are editing
                else{
                    res.render('links/edit', {
                        title: doc.name, 
                        link:doc,
                        categories:categories
                    });
                }
            }
        });
    }
    //if no query _id redirect to links
    else{
        req.flash("error", "Error editing link. Invalid link id.");
        res.redirect("/links");
    }
};

// routing for /links/delete?_id=XXXXXXXXXXXX
exports.delete = function(req, res){
    if(typeof req.query._id !== "undefined"){
        Link.find({_id:req.query._id}).remove(function(err){
            //if it can't delete, it just redirects /links
            if(err){
                console.log(err);
                req.flash("error", "Error deleting Link. Please try again.");
                res.redirect("/links");
            }
            //if successful delete, redirect to /links
            else{
                req.flash("notice", "Link successfully deleted.");
                res.redirect("/links");
            }
        });
    }
    //if no query _id redirect to links
    else{
        req.flash("error", "Error deleting link. Invalid link id.");
        res.redirect("/links");
    }
};
