//code runs on server start
var http = require("http");
var directions = require("directions");
var configs = require("./config");
var url= require("url");
var fs=require("fs");
var socketIO = require("socket.io");

var server = http.createServer(function(request,response){
    var urlObj = url.parse(request.url, true);
    var configuration = configs;
    configuration.query.origin = urlObj.query.origin;
    configuration.query.destination =urlObj.query.destination;

    //load string containing the html to serve the user
    var getPage = function(){
        fs.readFile("./html/index.html", function(err, data){
            if(err){
                console.log(err); //throw err;
            }
            //get the form and put it in where {{form}} is in index.html
            directions.getForm(function(res){
                data = data.toString();
                data = data.replace("{{form}}", res);
                data = data.replace("{{content}}", "Please enter a valid origin and destination.");
                response.writeHead(200,{"ContentType":"text/html"});
                response.end(data);
            });
        });
    }();
}).listen(process.env.PORT || 3000,process.env.IP || "127.0.0.1");

var io = socketIO.listen(server);
//connect
io.on('connection',function(socket){
    console.log('A new connection has been made: ' + socket.id);
    
    //disconnect
    socket.on('disconnect',function(){
    console.log("Disconnected " + this.id);
    this.broadcast.emit('info',{message:'A user has disconnected'});
    });
    
    //request for directions
    socket.on('getDirections', function(data){
        if(typeof data.origin !== "undefined" && typeof data.destination !== "undefined"){
            configs.query.origin = data.origin;
            configs.query.destination = data.destination;
            console.log(data.origin + " " + data.destination);
            //call directions api and return the response
            directions.getDirections(configs, function(res){
                socket.emit("resDirections", {response:res});
            });
        }
    });
});