exports.login = function(req, res){
  var responseObj = {title: 'Campus Solutions Login'};
  if(req.body.userid !== 'username' && req.body.pw !== 'password'){
    responseObj.loginError = "Username or Password incorrect";
  }
  responseObj.relatedLinks = [
    {href:"#", title:"Open Class Search", icon:"search"},
    {href:"#", title:"Browse Class Schedule", icon:"th-list"},
    {href:"#", title:"View Student Handbook", icon:"book"},
    {href:"#", title:"View Academic Calendar", icon:"calendar"},
    {href:"#", title:"Browse Course Catalogs", icon:"list-alt"}
    ];
  res.render('login', responseObj);
};
