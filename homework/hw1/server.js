//code runs on server start
var http = require("http");
var directions = require("directions");
var configs = require("./config");
var url= require("url");
var fs=require("fs");

http.createServer(function(request,response){
    var urlObj = url.parse(request.url, true);
    var configuration = configs;
    configuration.query.origin = urlObj.query.origin;
    configuration.query.destination =urlObj.query.destination;

    //load string containing the html to serve the user
    var getPage = function(){
        fs.readFile("./html/index.html", function(err, data){
            if(err){
                console.log(err); //throw err;
            }
            //get the form and put it in where {{form}} is in index.html
            directions.getForm(function(res){
                data = data.toString();
                data = data.replace("{{form}}", res);
                //if an origin and destination are in the url query
                if(typeof urlObj.query.origin !== 'undefined' && typeof urlObj.query.destination !== 'undefined'
                    && urlObj.query.origin !== '' && urlObj.query.destination !== ''){
                    //get directions from origin to destination and replace {{content}} with the directions
                    directions.getDirections(configuration, function(res2){
                        data = data.replace("{{content}}", res2);
                        response.writeHead(200,{"ContentType":"text/html"});
                        response.end(data);
                    });
                }
                //if an invailid or missing origin or destination
                else{
                    data = data.replace("{{content}}", "Please enter a valid origin and destination.");
                    response.writeHead(200,{"ContentType":"text/html"});
                    response.end(data);
                }
            });
        });
    };
    
    getPage();
}).listen(process.env.PORT || 3000,process.env.IP || "127.0.0.1");


