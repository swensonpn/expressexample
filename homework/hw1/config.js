console.log("loading config module");

var configuration = {
        host:'maps.googleapis.com',
        port:80,
        pathname:'/maps/api/directions/json',
        method:'GET',
        query:{'origin': '',
                'destination': ''
        }
};

module.exports = configuration;