
/*
 * GET users listing.
 */

exports.list = function(req, res){
  //res.send("respond with a resource");
  res.render('users/list',{
    title:'User List',
    users:[
      {text:'Spencer',href:'/users/view'},
      {text:'Jay',href:'/users/view'},
      {text:'Jake',href:'/users/view'}
    ]
  });
};

exports.my = function(req, res){
  res.send('<form action="/myusers" method="POST"><input type="submit" value="submit"/></form>');  
};

exports.post = function(req,res){
    res.send("We have a post");  
};