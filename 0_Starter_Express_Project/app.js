
/**
 * Module dependencies.
 */

var express = require('express')
  , routes = require('./routes')
  , user = require('./routes/users')
  , page = require('./routes/page')
  , http = require('http')
  , path = require('path')
  , mongoose = require('mongoose');

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

/**
 * Setting up and running MongoDB in Cloud9
 * 1. mkdir data
 * 2. echo 'mongod --bind_ip=$IP --dbpath=data --nojournal --rest "$@"' > mongod
 * 3. chmod a+x mongod
 * START
 * ./mongod
 * STOP
 * CNTRL + C
 **/
// Mongoose API http://mongoosejs.com/docs/api.html
mongoose.connect(process.env.IP);
mongoose.connection.on('connected',function(){
  console.log("Connected to MongoDB @" + process.env.IP);
});
mongoose.connection.on('disconnected',function(){
  console.log('Disconnected from MongoDB');
});
process.on('SIGINT',function(){
  mongoose.connection.close(function(){
    console.log('MongoDB disconnected due to Application Exit');
    process.exit(0);
  });
});

app.get('/', routes.index);
app.get('/users', user.list);
app.all('/users/add', user.add);
app.all('/users/edit/:id', user.edit);
app.all('/users/delete/:id', user.delete);
app.get('/pages/about', page.about);

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
